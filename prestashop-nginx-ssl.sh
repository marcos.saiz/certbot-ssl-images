#!/bin/bash
# Clouding.io

# Add repo
add-apt-repository ppa:certbot/certbot -y

# Update repos
apt update

# Install packages
apt install python-certbot-nginx -y

# Add domain
sed -i "s/"ubuntu-1804-image"/"$1"/g" /etc/nginx/sites-enabled/default

# Restart Nginx
systemctl restart nginx

# Register e-mail
yes | certbot --non-interactive register --agree-tos -m $2

# Create certificate
certbot --authenticator webroot -w /var/www/html --redirect --installer nginx -d $1
