## How to use

Adding execution premissions:

```
chmod u+x wp-nginx-ssl.sh
```

To generate certificate:

```
./wp-nginx-ssl.sh [domain] [e-mail]
```

Example

```
./wp-nginx-ssl.sh example.com admin@example.com
```
