#!/bin/bash
# Clouding.io

# Update repos
apt update

# Install packages
apt install certbot python3-certbot-nginx -y

# Add domain
sed -i "s/"ubuntu-2004-image"/"$1"/g" /etc/nginx/sites-enabled/default

# Restart Nginx
systemctl restart nginx

# Register e-mail
yes | certbot --non-interactive register --agree-tos -m $2

# Create certificate
certbot --authenticator webroot -w /var/www/html/magento2 --redirect --installer nginx -d $1
